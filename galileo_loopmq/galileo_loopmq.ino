


#include <SPI.h>
#include <Ethernet.h>
#include <ArduinoJson.h>
#include <stdlib.h>
#include <stdio.h>
#include <loopmq.h>
#include "configuration.h"

/*
Passing the defined information
*/
//IPAddress ip(192, 168, 1, 177);
byte mac[] = { 0x98, 0x4F, 0xEE, 0x05, 0x8E, 0xDB };
int port = port_number;                       // Port Number
char servername[] = server;                   // Server name
char c[] = clientID;                          // ClientID
char pass[]= passkey;                         // password
char user[] = userID;                         // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device  


EthernetClient client;
PubSubClient loopmq(client);                  // instance to use the loopmq functions.


void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
  
/*
Display the message published serially and on the LCD
*/
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);           // Serial Display
  }
     Serial.println();                        //Print on a new line
}


void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");     
    if (loopmq.connect(c, user, pass)) {                  // Attempt to connect
      delay(50);
    Serial.println("connected");              // Once connected, publish an announcement...
         
         loopmq.subscribe(s);                 // Subscribe to a topic  
         
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   
      /*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(5000);                                  // Wait 5 seconds before retrying
    }
  }
}


void setup() {
delay(30000);                                      // Wait for 30 secs for the device to connect to the network
Serial.begin(57600);                               // Start Serial
Ethernet.begin(mac);


  loopmq.setServer(servername, port);             // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);                   // Call the callbeck funciton when published  
  
  
 //delay(1500);
 }

void loop() {


 /*  
  JSON parser
  */
  StaticJsonBuffer<200> jsonBuffer;                   //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();      // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Intel Galileo";    // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;


  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Status"]="Litmus loop";


  //root.printTo(Serial);                              // prints to serial terminal
  //Serial.println();

  char buffer[200];                                 // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));             // copy the JSON to the buffer to pass as a payload 
  /*
  Publish to the server
  */

  if (!loopmq.connected()) {
    reconnect();                                    // Try to reconnect if connection dropped
  }
  if (loopmq.connect(c, user, pass)){  
   loopmq.publish(p,buffer);                        // Publish message to the server    
  }

 loopmq.loop();                                   // check if the network is connected and also if the client is up and running

}

