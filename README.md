# Galileo-arduino

The **Intel Galileo** is a tiny computer-on-module offered by Intel as a development system for wearable devices[1] and Internet of Things devices. 


## Overview 

The project demonstrates the implementation of MQTT.**Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth,
high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication.It plays an important role in the internet of things.
MQTT protocol works on the **TCP/IP connection**.

The library provides an example of publish and subscribe messaging with a server that supports MQTT using Intel Edison- Arduino.
 
*Features provided by the client library*

* *Connect* the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* *Publish* any message to the MQTT server in standard JSON format on a user soecified topic
* *Subscribe* data from the server to the device on a specific topic
* *Unsubscribe* the topic to stop streaming data to the cloud
* *Disconnect* the device from any network connectivity.

The following Table shows status of the client and the server when the above functions are implemented.

|Function	|  Server Status   |    Client Status
----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |	Connected|
|Loopmq.subscribe |	Connected |	Connected|
|Loopmq.unsubscribe |	Connected |	Disconnected|
|Loopmq.disconnect |	Disconnected |	Disconnected|


## Getting Started with Intel Edison- Arduino

A step by step instruction to setup the Intel Edison board can be followed at the [link](https://software.intel.com/en-us/get-started-galileo-windows)

Basic steps to start using the Edison board are given below:

* STEP 1: Assemble and connect the board as shown in the figure.
 ![Alt text](https://bytebucket.org/litmusloopdocs/intel-galileo/raw/master/extras/setup.png)

* STEP 2: Prepare the microSD card with the OS as seen in the [link](https://software.intel.com/en-us/get-started-galileo-windows-step1) 

The device is connected to the network from the console once the device is connected to [serial](https://software.intel.com/en-us/get-started-galileo-windows-step3) interface.

* STEP 3: Install Arduino IDE and install Galileo board from the board manager. 

* STEP 4: Install the library or open the galileo_loopmq.ino file from the examples.

* STEP 5: Connect any sensor to Galileo and start sending data to the cloud

## Configuration

The user need to define a list of parameters in order to connect the device to a server in a secured manner.

Below are the list of minimum definitions required by the user to send data to the cloud.

```
#define port_number 1883                              // Port number
#define server "liveiot.mq.litmusloop.com"            // Server name
#define clientID "galileo"                            // ClientID
#define passkey "8Hgw*j#978pT"                        // password
#define userID "demoroom"                             // username 
#define subTOPIC "demoroom/galileo/pub"               // Subscribe on this topic to get the data
#define pubTopic "demoroom/galileo/sub"               // Publish on this tpoic to send data or command to device 
```

The mac address and/or IP is required for the device to connect to network.

```
byte mac[] = { 0x98, 0x4F, 0xEE, 0x05, 0x8E, 0xDB };

```

## Functions

1.***loopmq.connect (client ID)***
This function is used to connect the device or the client to the client ID specified by the user.

```
if (loopmq.connect(c)) {
      Serial.println ("connected"); 
```       

2.***loopmq.connect (client ID, username, password)***
Checks for the username and password specified by the user to connect the device to the network.

```
if (loopmq.connect(c, user, pass))
```
 

3.***loopmq.publish (topic, data)***

This function is used to publish data in string format to the topic specified by the user. 

In this example numbers form 1-100 are published in a continuous loop. You can also monitor the data that is send to the cloud, serially.

```
loopmq.publish (p,buffer);        // Publish message to the server
```


4.***loopmq.subscribe (topic)***
This function is used to subscribe to a topic to which data will be published to the device.

```
loopmq.subscribe(s);                    // Subscribe to a topic
```

5.***loopmq.unsubscribe (topic)***
This function is used to unsubscribe the device from the server. Calling this function will stop sending messages from the device to the server.

```
// loopmq.unsubscribe(s);   Note: uncomment the code to unsubscribe from the topic
```
 
6.***lopmq.disconnect ()***

This function is used to disconnect the device from the cloud or server. Disconnect does not stop the functionality of the device but disconnects it from the network. The device works fine locally but does not send any update to the internet.

```
// loopmq.disconnect();  Note: uncomment the code to disconnect the device
```

7.***JSON PARSER***

This function is used to create a JSON payload to be passed to the broker as payload. Please refer the [link](https://github.com/bblanchon/ArduinoJson/wiki/Compatibility-issues) for any compalibility issues.

```
  StaticJsonBuffer<200> jsonBuffer;                //Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase the size 

  JsonObject& root = jsonBuffer.createObject();      // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "Intel Galileo";                 // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;


  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Status"]="Litmus loop";

```